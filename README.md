# Skyking Dark

## About
A dark skin with mascot for websdr.ewi.utwente.nl:8901 and websdr.org. This style is an expansion of the one by 2E0PGS.

## Links
Userystyles page: https://userstyles.org/styles/148538/skyking-dark-websdr-utwenti-websdr-org

## Screenshots
### Utwenti
![utwenti](websdr-2.png)

### WebSDR.org
![websdr.org](websdr-org.png)